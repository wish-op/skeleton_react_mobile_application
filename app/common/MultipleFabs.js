import React, { Component, PropTypes } from 'react'

import FloatingActionButton from 'material-ui/FloatingActionButton'
import FontIcon from 'material-ui/FontIcon'

const styles = {
  container: {
    position: 'fixed',
    right: 16,
    bottom: 16
  },
  mainFab: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    zIndex: 2
  },
  miniFabStart: {
    position: 'absolute',
    right: 8,
    bottom: 8,
    zIndex: 1
  },
  miniFabFinish1: {
    position: 'absolute',
    right: 8,
    bottom: 88,
    zIndex: 1
  },
  miniFabFinish2: {
    position: 'absolute',
    right: 78,
    bottom: 78,
    zIndex: 1
  },
  miniFabFinish3: {
    position: 'absolute',
    right: 88,
    bottom: 8,
    zIndex: 1
  },
}

class MultipleFabs extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }

  _toggleFab = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const {
      openIcon,
      closeIcon
    } = this.props

    let isOpen = this.state.open

    return (
      <div style={styles.container}>
        <FloatingActionButton
          style={styles.mainFab}
          secondary={true}
          onTouchTap={this._toggleFab}
        >
          {isOpen ? closeIcon : openIcon}
        </FloatingActionButton>
        <FloatingActionButton
          mini={true}
          style={!isOpen ? styles.miniFabStart : styles.miniFabFinish1}
        >
          <FontIcon className='material-icons'>directions_car</FontIcon>
        </FloatingActionButton>
        <FloatingActionButton
          mini={true}
          style={!isOpen ? styles.miniFabStart : styles.miniFabFinish2}
        >
          <FontIcon className='material-icons'>phone</FontIcon>
        </FloatingActionButton>
        <FloatingActionButton
          mini={true}
          style={!isOpen ? styles.miniFabStart : styles.miniFabFinish3}
        >
          <FontIcon className='material-icons'>email</FontIcon>
        </FloatingActionButton>
      </div>
    )
  }
}

MultipleFabs.propTypes = {
  openIcon: PropTypes.object.isRequired,
  closeIcon: PropTypes.object.isRequired,
}

MultipleFabs.defaultProps = {
  openIcon: (<FontIcon className='material-icons'>navigation</FontIcon>),
  closeIcon: (<FontIcon className='material-icons'>close</FontIcon>),
}

export default MultipleFabs
