// React
import React, { Component, PropTypes } from 'react'

// Styling
const styles = {
  img: {
    width: '100%',
    height: 'auto',
    transition: 'opacity 500ms'
  }
}

class LazyImg extends Component {
  constructor(props) {
    super(props)
    this.state = {
      opacity: 0
    }
    this._onLoad = this._onLoad.bind(this)
  }

  _onLoad() {
    this.setState({
      opacity: 1
    })
  }

  render() {
    // Get props
    const { src, size, style } = this.props

    // Get state
    let opacity = this.state.opacity

    return (
      <div style={{...size, ...style}}>
        <img src={src}
          style={{ ...styles.img, opacity}}
          onLoad={this._onLoad}/>
      </div>
    )
  }
}

LazyImg.defaultProps = {
  src: 'http://placeskull.com/150/150',
  size: { width: 150, height: 150 }
}

LazyImg.propTypes = {
  src: PropTypes.string.isRequired,
  size: PropTypes.shape({
    width: React.PropTypes.any,
    height: React.PropTypes.any
  }).isRequired
}

export default LazyImg
