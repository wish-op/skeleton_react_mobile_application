import React, { PropTypes } from 'react'

const styles = {
  page: {
    height: '100vh',
    width: '100wh',
    display: 'flex',
    flexDirection: 'column'
  }
}

const Page = ({children}) => (
  <div style={styles.page}>
    {
      children
    }
  </div>
)

Page.defaultProps = {}

Page.propTypes = {}

export default Page
