import React, { Component, PropTypes } from 'react'

const styles = {
  appBar: {
    flex: '0 0 auto'
  }
}

const PageRow = ({children, style}) => (
  <div style={{...styles.appBar, ...style}}>
    {
      children
    }
  </div>
)

PageRow.defaultProps = {}

PageRow.propTypes = {}

export default PageRow
