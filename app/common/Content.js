import React, { PropTypes } from 'react'

const styles = {
  content: {
    flex: '1 1 auto',
    overflowY: 'auto'
  }
}

const Content = ({children}) => (
  <div style={styles.content}>
    {
      children
    }
  </div>
)

Content.defaultProps = {}

Content.propTypes = {}

export default Content
