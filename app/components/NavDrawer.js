// React
import React, { Component, PropTypes } from 'react'

// Material UI
import Drawer from 'material-ui/Drawer'
import Paper from 'material-ui/Paper'
import { indigo500, green500, white } from 'material-ui/styles/colors'
import Avatar from 'material-ui/Avatar'
import MenuItem from 'material-ui/MenuItem'
import FontIcon from 'material-ui/FontIcon'

// Styling
const styles = {
  paper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    width: '100%',
    height: 200,
    background: 'linear-gradient(145deg,' + indigo500 + ',' + green500 + ')'
  },
  userContainer: {
    margin: 12
  },
  avatar: {
    borderRadius: '100%',
    border: '2px solid ' + white
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    color: white,
    margin: '8px 0px'
  },
  email: {
    fontSize: 14,
    fontStyle: 'italic',
    color: white,
    margin: '8px 0px'
  }
}

const NavDrawer = ({open, switchDrawer}) => (
  <Drawer
    docked={false}
    width={240}
    open={open}
    onRequestChange={() => {
      switchDrawer(!open)
    }}>
    <Paper
      style={styles.paper}
      zDepth={1}
    >
      <div style={styles.userContainer}>
        <img style={styles.avatar}
          src='http://placeskull.com/55/55'
        />
        <p style={styles.name}>Nome Cognome</p>
        <p style={styles.email}>user@mail.com</p>
      </div>
    </Paper>
    <MenuItem
      primaryText='Item 1'
      leftIcon={<FontIcon className='material-icons'>face</FontIcon>}
    />
    <MenuItem
      primaryText='Item 2'
      leftIcon={<FontIcon className='material-icons'>face</FontIcon>}
    />
    <MenuItem
      primaryText='Item 3'
      leftIcon={<FontIcon className='material-icons'>face</FontIcon>}
    />
  </Drawer>
)

export default NavDrawer
