// React
import React from 'react'
import { render } from 'react-dom'

// Components
import App from './containers/App'
import PageOne from './containers/PageOne'

// Material UI
import injectTapEventPlugin from 'react-tap-event-plugin'
import {
  indigo500,
  indigo700,
  indigo400,
  green100,
  greenA200,
  green500
} from 'material-ui/styles/colors'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

// Routing
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

// Store
import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

const store = configureStore(hashHistory)
const history = syncHistoryWithStore(hashHistory, store)

injectTapEventPlugin()

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: indigo500,
    primary2Color: indigo700,
    primary3Color: indigo400,
    accent1Color: greenA200,
    accent2Color: green100,
    accent3Color: green500,
  },
})

// Cordova event listeners
document.addEventListener("deviceready", () => {
  // Render app as soon as device is ready
  renderApp()
},
false)

const renderApp = () => {
  render(
    <Provider store={store}>
      <MuiThemeProvider muiTheme={muiTheme}>
        <Router history={history}>
          <Route path="/" component={App}>
            <IndexRoute component={PageOne}></IndexRoute>
          </Route>
        </Router>
      </MuiThemeProvider>
    </Provider>,
    document.getElementById('app_content')
  )
}
