// React
import React, { Component, PropTypes } from 'react'

// Redux utilities
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Actions creators
import { switchDrawer } from '../actions/DrawerActions'

// Material UI
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import { Tabs, Tab } from 'material-ui/Tabs'
import {
  BottomNavigation,
  BottomNavigationItem
} from 'material-ui/BottomNavigation'
import FontIcon from 'material-ui/FontIcon'

// Utils
import Page from '../common/Page'
import Content from '../common/Content'
import PageRow from '../common/PageRow'

class PageOne extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
      // state props
      // action props
      switchDrawer
    } = this.props

    // App bar burger
    let burger = (
      <IconButton
        onTouchTap={() => { switchDrawer(true) }}
        >
        <FontIcon className='material-icons'>menu</FontIcon>
      </IconButton>
    )

    return (
      <Page>
        <PageRow>
          <AppBar title='City Events' zDepth={0} iconElementLeft={burger} />
        </PageRow>
        <PageRow>
          <Tabs>
            <Tab label='Tab 1'/>
            <Tab label='Tab 2'/>
            <Tab label='Tab 3'/>
          </Tabs>
        </PageRow>
        <Content>
          {/* Content here */}
        </Content>
        <PageRow>
          <BottomNavigation>
            <BottomNavigationItem
              label='Item 1'
              icon={<FontIcon className='material-icons'>near_me</FontIcon>}
            />
            <BottomNavigationItem
              label='Item 2'
              icon={<FontIcon className='material-icons'>spa</FontIcon>}
            />
            <BottomNavigationItem
              label='Item 3'
              icon={<FontIcon className='material-icons'>account_balance</FontIcon>}
            />
          </BottomNavigation>
        </PageRow>
      </Page>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    switchDrawer
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageOne)
