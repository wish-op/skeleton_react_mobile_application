// React
import React, { Component } from 'react'

// Components
import NavDrawer from '../components/NavDrawer'

// Action creators
import {
  switchDrawer
} from '../actions/DrawerActions'

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
      // state props
      children,
      isDrawerOpen,
      // action props
      switchDrawer
    } = this.props

    return (
      <div>
        <NavDrawer
          open={isDrawerOpen}
          switchDrawer={switchDrawer}
        />
        {
          children
        }
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isDrawerOpen: state.drawer.isOpen
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    switchDrawer
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
