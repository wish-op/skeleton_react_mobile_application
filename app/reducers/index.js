import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// My reducers
import drawer from './DrawerReducer'

const rootReducer = combineReducers({
  routing: routerReducer,
  drawer
})

export default rootReducer
