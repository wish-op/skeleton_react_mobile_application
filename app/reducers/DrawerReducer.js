// Actions
import {
  SWITCH_DRAWER
} from '../actions/DrawerActions'

let defaultState = {
  isOpen: false
}

const drawer = (state = defaultState, action) => {
  switch (action.type) {
    case SWITCH_DRAWER:
      return {
        ...state,
        isOpen: action.open
      }
    default:
      return {
        ...state
      }
  }
}

export default drawer
