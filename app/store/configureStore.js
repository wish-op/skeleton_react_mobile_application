// Redux utilities
import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'

// Middlewares
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

// Store enhancer
import persistState from 'redux-localstorage'
import handleTransitions from 'redux-history-transitions'

// Reducers
import rootReducer from '../reducers/index'

const configureStore = (history) => {
  return createStore(
    rootReducer,
    compose(
      //persistState(['', ''], { key:''}),
      handleTransitions(history),
      applyMiddleware(thunk, createLogger())
    )
  )
}

export default configureStore
