// Action constants
export const SWITCH_DRAWER = "SWITCH_DRAWER"

// Action creators
export const switchDrawer = (open) => {
  return {
    type: SWITCH_DRAWER,
    open
  }
}
