const path = require('path');
const webpack = require('webpack');

const PATHS = {
  app: path.join(__dirname, 'app/index.js'),
  dist: path.join(__dirname, 'www/libs'),
  android_base: path.join(__dirname, 'platforms/android/assets/www')
};

module.exports = {
  entry: PATHS.app,
  output: {
    path: PATHS.dist,
    publicPath: '/libs/',
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devtool: 'source-map',
  devServer: {
    contentBase: PATHS.android_base,
    stats: 'errors-only',
    hot: true,
    inline: true
  }
}
