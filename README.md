# React App Starter Kit
A simple boilerplate to start from when developing cordova hybrid apps using React and Redux.

## Prerequisites
1. Java JDK
2. Android Studio & Android SDK
3. install cordova globally: `npm install -g cordova`
4. Add android platform directory: `cordova platform add android`
5. Ripple Emulator (optional) in order to test in the browser
6. Install dependencies locally: `npm install`
7. Copy assets into android platform: `npm run prepare`

## Start a web server
Now that the enviroment is set up, you can start the webpack-dev-server installed previously as a dependecy typing `npm start`. The webserver is configured to watch for file changes and push them to the app with hot reloading.

## Run on Android device
To run the app on an Android device all the javascript files need to be bundled in a single file that will be loaded in the html page, and all the changes need to be copied in the android platform folder. You can do this typing `npm run android`.

# Tips and Tricks

## SoftKeyboard overlay mode
In the platform/android folder open the manifest.xml and in the main activity
tag replace the android:windowSoftInputMode="adjustResize" attribute with android:windowSoftInputMode="adjustPan"

## Crosswalk single output
- $> cd platforms/android
- $> echo “ext.cdvBuildMultipleApks=false” >> build­extras.gradle
